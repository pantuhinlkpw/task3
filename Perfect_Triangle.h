#pragma once
#include <iostream>
#include "Perfect.h"
#include "Triangle.h"

class Perfect_Triangle: public Perfect, public Triangle {
public:
	Perfect_Triangle();
	Perfect_Triangle(double x1, double y1, double x2, double y2, double x3, double y3);
	double Square() const;
	void Out() const;

};
