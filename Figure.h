#pragma once
#include <iostream>

class Figure {
public:
	virtual double Square() const = 0;
};

