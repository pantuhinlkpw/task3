#include <iostream>
#include <cmath>
#include "Triangle.h"

Triangle::Triangle() {}

Triangle::Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
	point1[0] = x1;
	point1[1] = y1;
	point2[0] = x2;
	point2[1] = y2;
	point3[0] = x3;
	point3[1] = y3;
}
double Triangle::Square() const {
	double a = sqrt(pow(point1[0] - point2[0], 2) + pow(point1[1] - point2[1], 2));
	double b = sqrt(pow(point1[0] - point3[0], 2) + pow(point1[1] - point3[1], 2));
	double c = sqrt(pow(point3[0] - point2[0], 2) + pow(point3[1] - point2[1], 2));
	double p = (a + b + c) / 2;
	return sqrt(p * (p - a) * (p - b) * (p - c));
}

void Triangle::Out() const {
	std::cout << "point1: " << point1[0] << " " << point1[1] << std::endl;
	std::cout << "point2: " << point2[0] << " " << point2[1] << std::endl;
	std::cout << "point3: " << point3[0] << " " << point3[1] << std::endl;
}