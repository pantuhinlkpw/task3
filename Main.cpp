#include <iostream>
#include "Figure.h"
#include "Perfect.h"
#include "Triangle.h"
#include "Perfect_Triangle.h"

int main() {
	Triangle a(1, 0, 0, 1, 0, 0);
	Perfect_Triangle b(0, 0, 4, 0, 2, 3.4);
	a.Out();
	std::cout << " " << std::endl;
	std::cout << a.Square() << std::endl;
	std::cout << " " << std::endl;
	b.Out();
	std::cout << " " << std::endl;
	std::cout << b.Square() << std::endl;
	return 0;
}