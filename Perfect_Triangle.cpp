#include <iostream>
#include <cmath>
#include "Perfect_Triangle.h"

Perfect_Triangle::Perfect_Triangle() {}

Perfect_Triangle::Perfect_Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
	cout_sides = 3;
	point1[0] = x1;
	point1[1] = y1;
	point2[0] = x2;
	point2[1] = y2;
	point3[0] = x3;
	point3[1] = y3;
}

double Perfect_Triangle::Square() const {
	double a = sqrt(pow(point1[0] - point2[0], 2) + pow(point1[1] - point2[1], 2));
	return sqrt(3) * a * a / 4;
}

void Perfect_Triangle::Out() const {
	std::cout << "int: " << cout_sides << std::endl;
	std::cout << "point1: " << point1[0] << " " << point1[1] << std::endl;
	std::cout << "point2: " << point2[0] << " " << point2[1] << std::endl;
	std::cout << "point3: " << point3[0] << " " << point3[1] << std::endl;
}