PROG = Classes
PROG1 = Figure
PROG2 = Perfect
PROG3 = Triangle
PROG4 = Perfect_Triangle

comp: $(PROG2).o $(PROG3).o $(PROG4).o Main.o
	g++ -g -Wall Main.o $(PROG2).o $(PROG3).o $(PROG4).o -o $(PROG)
Main.o: Main.cpp
	g++ -c -g -Wall Main.cpp -o Main.o
$(PROG2).o: $(PROG2).cpp
	g++ -c -g -Wall $(PROG2).cpp -o $(PROG2).o
$(PROG3).o: $(PROG3).cpp
	g++ -c -g -Wall $(PROG3).cpp -o $(PROG3).o
$(PROG4).o: $(PROG4).cpp
	g++ -c -g -Wall $(PROG4).cpp -o $(PROG4).o
run:
	./$(PROG)
clean:
	rm -f *.o $(PROG)