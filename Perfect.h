#pragma once
#include <iostream>
#include "Figure.h"

class Perfect: public Figure {
protected:
	int cout_sides;
public:
	Perfect();
	Perfect (int x);
	double Square() const;
};
