#pragma once
#include <iostream>
#include "Figure.h"

class Triangle: public Figure {
protected:
	double point1[2];
	double point2[2];
	double point3[2];
public:
	Triangle();
	Triangle(double x1, double y1, double x2, double y2, double x3, double y3);
	double Square() const;
	virtual void Out() const;
};
